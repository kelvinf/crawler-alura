const axios = require('axios').default
const fs = require('fs')
const path = require('path')
const { URLSearchParams } = require("url")
const { cookie, qualidade } = require('./config.json')
const { to, parseDom } = require('./utils')
const ALURA_HOST = "https://cursos.alura.com.br"

const params = {
  headers: {
    Cookie: `caelum.login.token=${cookie}`,
    "autority": "cursos.alura.com.br",
    "method": "GET",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
  }
}

exports.getAllVideoLinksInSection = async function (url) {

  console.log("Buscando todos os vídeos da URL:", url)
  return axios.get(url, params)
    .then(response => {
      const $ = parseDom(response.data)
      const allVideoLinks = $("a.task-menu-nav-item-link-VIDEO")
      const allVideoLinksParsed = Array.prototype.map.call(allVideoLinks, el => "https://cursos.alura.com.br" + el.href + "/video")
      console.log("Links encontrados:", allVideoLinksParsed)
      return allVideoLinksParsed
    })
    .catch(response => {
      console.error("Erro ao abrir url", url)
      throw response
    })
}

exports.getAllLinksLessons = async function (url) {
  console.log("Buscando todos os links da URL:", url)
  const [error, response] = await to(axios.get(url, params))
  if (error) {
    console.error("Erro ao abrir url", url)
    throw error
  }
  const $ = parseDom(response.data)
  const allLinksLessons = $("a.courseSectionList-section")
  const allLinksLessonsParsed = Array.prototype.map.call(allLinksLessons, el => ALURA_HOST + el.href)
  console.log("Links encontrados", allLinksLessonsParsed)
  return allLinksLessonsParsed
}

exports.getTitleCourse = async function (url) {
  const [error, response] = await to(axios.get(url, params))
  if (error) {
    console.error("Erro ao abrir url", url)
    throw error
  }
  const $ = parseDom(response.data)
  const titleElement = $('h1.course-header-banner-title strong')
  return titleElement.text()
}

exports.getJsonVimeo = async function (url) {
  console.log("Buscando JSON do Vimeo, URL:", url)
  console.log("Qualidade:", qualidade)

  const [error, response] = await to(axios.get(url, params))

  if (error) {
    console.error("Erro ao abrir url", url)
    throw error
  }

  if (!response.data || response.data.length === 0) {
    throw new Error("Lista de links vazia")
  }

  let { link } = response.data.find(el => el.quality === qualidade) || {}
  if (!link) {
    link = response.data[0].link
  }
  console.log("Link obtido:", link)
  return link

}

exports.downloadVimeo = async function (url, dirPath) {
  const fileName = new URLSearchParams(url).get('filename').replace(/\+/g, " ");
  return axios({
    url: url,
    method: 'GET',
    responseType: 'stream',
  })
    .then(response => {
      const pathToSaveVideo = buildPathToSave(dirPath)
      const writer = response.data.pipe(fs.createWriteStream(path.join(pathToSaveVideo, fileName)))

      return new Promise((resolve, reject) => {

        writer.on('finish', () => {
          console.log('Download completo do arquivo:', fileName);
          console.log('URL:', url);
          resolve()
        });

        writer.on('error', () => {
          console.log('Falha ao salvar o arquivo:', fileName);
          console.log('URL:', url);
          reject()
        });

      })
    })
    .catch(error => {
      console.error("Falha ao efetuar download")
      console.error("URL:", url)
      console.error("Arquivo:", fileName)
      throw error
    })

}


exports.getAllCoursesInCategory = async function (url) {
  console.log("Buscando todos os cursos da URL:", url)
  const [error, response] = await to(axios.get(url, params))
  if (error) {
    console.error("Erro ao abrir url", url)
    throw error
  }
  const $ = parseDom(response.data)
  const allCoursesLinkElement = $('.course-card__course-link')
  const allCoursesLink = Array.prototype.map.call(allCoursesLinkElement, el => ALURA_HOST + el.href)
  console.log("Cursos encontrados:", allCoursesLink)
  return allCoursesLink
}

function buildPathToSave(dirName) {
  const safeDirName = dirName.replace(/\:/g, " -")
  const pathToSaveVideo = path.join(__dirname, "downloads", safeDirName)
  createDirPath(pathToSaveVideo)
  return pathToSaveVideo
}

function createDirPath(path) {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path)
  }
}
