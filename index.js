const Promise = require('bluebird')
const { shuffle } = require('./utils')
const {
  getAllVideoLinksInSection,
  getAllLinksLessons,
  getJsonVimeo,
  downloadVimeo,
  getAllCoursesInCategory,
  getTitleCourse
} = require('./crawler')
const { categoryLink } = require('./config.json')
const RANDOM_60_MIN = (Math.random() * 1800000) + 1000
const RANDOM_5_SECONDS = (Math.random() * 5000) + 1000
const RANDOM_10_SECONDS = (Math.random() * 10000) + 1000
const _30_SECONDS = 30000

getAllCoursesInCategory(categoryLink)
  .then(courses => {
    const timeout = RANDOM_60_MIN
    async function recursive(course) {
      if (course) {
        const title = await getTitleCourse(course)
        return getAllLinksLessons(course)
          .then(getSectionLinks)
          .then(getAllLinks)
          .then(response => download(response, title))
          .then(() => Promise.delay(timeout))
          .then(() => courses.pop())
          .then(course => recursive(course))
      }
      else {
        console.log("Todos os cursos foram baixados!")
      }
    }

    return recursive(courses.pop())

  })


function getSectionLinks(items) {
  console.log("Embaralhando Lista")
  const randomizedItens = shuffle(items) //Downloads sequenciais geram um bloqueio da conta

  let allLinks = []
  const timeout = 0

  async function recursive(item) {
    if (item) {
      return getAllVideoLinksInSection(item)
        .then(response => allLinks.push(...response))
        .then(() => Promise.delay(timeout))
        .then(() => randomizedItens.pop())
        .then(item => recursive(item))
    }
    else {
      return allLinks
    }
  }

  return recursive(randomizedItens.pop())

}

function getAllLinks(allLinks) {
  const randomizedItens = shuffle(allLinks)
  let linksVimeo = []
  const timeout = RANDOM_5_SECONDS

  async function recursive(item) {
    if (item) {
      return getJsonVimeo(item)
        .then(response => linksVimeo.push(response))
        .then(() => Promise.delay(timeout))
        .then(() => randomizedItens.pop())
        .then(item => recursive(item))
    }
    else {
      return linksVimeo
    }
  }

  return recursive(randomizedItens.pop())

}

async function download(allVimeoLinks, dirName) {
  const randomizedItens = shuffle(allVimeoLinks)
  const timeout = RANDOM_10_SECONDS + _30_SECONDS

  async function recursive(item) {
    if (item) {
      return downloadVimeo(item, dirName)
        .then(() => Promise.delay(timeout))
        .then(() => randomizedItens.pop())
        .then(item => recursive(item))
    }
    else {
      console.log("Todos os download finalizados!")
    }
  }

  return recursive(randomizedItens.pop())

}