const Jquery = require('jquery')
const { JSDOM } = require("jsdom")

async function to(promise) {
    return promise.then(response => [null, response]).catch(e => [e, null])
}

function parseDom(response) {
    const { window } = new JSDOM(response)
    const $ = Jquery(window)
    return $
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

module.exports = { to, shuffle, parseDom }